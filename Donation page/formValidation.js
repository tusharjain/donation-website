let form = document.querySelector("form");

let firstname = document.querySelector("#firstname");
let lastname = document.querySelector("#lastname");
let street = document.querySelector("#street");
let city = document.querySelector("#city");
let state = document.querySelector("#state");
let zipcode = document.querySelector("#zipcode");
let email = document.querySelector("#email");

let creditcard = document.querySelector("#creditcard");
let expire = document.querySelector("#expire");
let code = document.querySelector("#code");

let errorBoxTop = document.querySelector(".errorMessageTop span");
let errorBoxBottom = document.querySelector(".errorMessageBottom span");

let successBox = document.querySelector(".successMessage span");

form.addEventListener("submit", validate);
function validate(event) {
    let errorMessageTop = "";
    let errorMessageBottom = ""; 
    let successMessage = "";

    if(firstname.value === "") {
        errorMessageTop = "First name cannot be blank."
        firstname.focus(); 
    }
    else if (city.value.match( /^[0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+$/)) {
        errorMessageTop = "Enter proper city name";
        city.focus(); 
    }
    else if (state.value.match(/^[0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+$/)) {
        errorMessageTop = "Enter proper state name";
        state.focus(); 
    }
    else if (zipcode.value.match(/^[a-zA-Z!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+$/) || (zipcode.value.length !== 0 && zipcode.value.length !== 6 )) {
        errorMessageTop = "Enter proper zipcode";
        zipcode.focus(); 
    }
    else if (email.value === "" || !email.value.match( /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
        if(email.value === "") {
            errorMessageTop = "Email cannot be blank."
        }
        else {
            errorMessageTop = "Enter proper email";
        }
        
        email.focus(); 
    }
    else if (creditcard.value === "" || !creditcard.value.match(/\d{4}-\d{4}-\d{4}-\d{4}/) || creditcard.value.length > 19) {
        if(creditcard.value === "") {
            errorMessageBottom = "Credit Card Number cannot be blank."
        }
        else {
            errorMessageBottom = "Enter proper Credit Card Number";
        }
        creditcard.focus(); 
    }
    else if (expire.value === "" || !expire.value.match(/\d{2}\/\d{4}/) || expire.value.length > 7) {
        if(expire.value === "") {
            errorMessageBottom = "Expiry date cannot be blank."
        }
        else {
            errorMessageBottom = "Enter proper expiry date";
        }
        expire.focus(); 
    }
    else if (code.value === "" || !code.value.match(/\d{3}/) || code.value.length > 3) {
        if(code.value === "") {
            errorMessageBottom = "CVV be blank."
        }
        else {
            errorMessageBottom = "Enter proper CVV";
        }
        code.focus(); 
    }
    else {
      errorMessageBottom = ""; 
      errorMessageTop = "";
      successMessage = "Form Submitted!"
    }

    errorBoxTop.innerText = errorMessageTop;
    errorBoxBottom.innerText = errorMessageBottom;
    successBox.innerText = successMessage;

    if(successMessage) {
        form.reset();
    }

    event.preventDefault(); 
}



